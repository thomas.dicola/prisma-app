import { NextApiRequest, NextApiResponse } from 'next';
import prisma from '../../lib/prisma';

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { label, picture } = req.body;
  const result = await prisma.item.create({
    data: {
      label: label,
      picture: picture,
    },
  });
  res.json(result);
}
